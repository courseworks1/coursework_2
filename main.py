def main():
    mass = [1, 5, 8, 3, 2, 4, 6, 9, 7]
    easy(mass)
    print(str(mass))


def easy(mass):
    for i in range(len(mass) - 1):
        j = i - 1
        temp = mass[i]
        while mass[j] > temp and j >= 0:
            mass[j + 1] = mass[j]
            j -= 1
        mass[j + 1] = temp
    return mass


main()
